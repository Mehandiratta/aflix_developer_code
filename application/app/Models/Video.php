<?php




namespace HelloVideo\Models;


use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Comments\Traits\Comments;

class Video extends Model {

	use Comments;
	protected $guarded = array();


	public static $rules = array();

	protected $fillable = array('user_id', 'video_category_id', 'title', 'type', 'access', 'details', 'description', 'active', 'featured', 'duration', 'image', 'ads' ,'ass','subtitle','ads_link', 'delbycontri','embed_code', 'mp4_url', 'webm_url', 'ogg_url', 'created_at','price','validate1','trailor_embed_code','live','type1','subtitle','bannerImg');

	public function tags(){
		return $this->belongsToMany(Tag::class);
	}

	public function ad()
	{
		return $this->hasOne(Ad::class ,'id', 'ads');
	}
}
