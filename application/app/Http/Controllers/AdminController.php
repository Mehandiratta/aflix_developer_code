<?php

namespace HelloVideo\Http\Controllers;


use HelloVideo\User;
use HelloVideo\Models\Setting;
use HelloVideo\Models\Video;
use HelloVideo\Models\Post;

use Auth;
use Carbon\Carbon as Carbon;

class AdminController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	public function index()
	{
		$start = (new Carbon('now'))->hour(0)->minute(0)->second(0);
		$end = (new Carbon('now'))->hour(23)->minute(59)->second(59);

		$total_subscribers = count(User::where('active', '=', 1)->where('role', '=', 'subscriber')->where('stripe_active', '=', 1)->get());
		$new_subscribers = count(User::where('active', '=', 1)->where('role', '=', 'subscriber')->where('stripe_active', '=', 1)->whereBetween('created_at', [$start, $end])->get());
		$total_videos = count(Video::where('active', '=', 1)->get());
		$total_posts = count(Post::where('active', '=', 1)->get());

		$today_registered = User::where('role' , 'registered')
		                        ->whereDay('created_at', '=', date('d'))
														->whereMonth('created_at', '=', date('m'))
                            ->whereYear('created_at', '=', date('Y'))
														->count();

		$today_subscriberd = User::where('role' , 'subscriber')
		                        ->whereDay('created_at', '=', date('d'))
														->whereMonth('created_at', '=', date('m'))
                            ->whereYear('created_at', '=', date('Y'))
														->count();


		$settings = Setting::first();


		$data = array(
			'admin_user' => Auth::user(),
			'total_subscribers' => $total_subscribers,
			'new_subscribers' => $new_subscribers,
			'total_videos' => $total_videos,
			'total_posts' => $total_posts,
			'settings' => $settings,
			'today_registered' => $today_registered,
			'today_subscriberd' =>$today_subscriberd
			);
		return view('admin.index',$data );
	}


	public function settings_form(){
		$settings = Setting::first();
		$user = Auth::user();
		$data = array(
			'settings' => $settings,
			'admin_user'	=> $user,
			);
		return view('admin.settings.index', $data );
	}

}
